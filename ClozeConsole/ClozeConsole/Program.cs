﻿using pidroh.conceptmap.model;
using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBuild
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            RandomSupplier.Generate = () =>
            {
                return (float)rnd.NextDouble();
            };

            var map = ConceptMapDataUtils.LoadFromJson(File.ReadAllText("komodo.txt"));
            var questions = new ClozeGenerationMap().GetQuestions(map);
            var qs = new QuestionScheduler();
            qs.Initialize(questions.Length);
            while (qs.HasQuestion())
            {
                var qid = qs.Next();
                var question = questions[qid];
                Console.Clear();
                var items = question.GetRandomizedItems();
                Console.WriteLine(question.prompt);
                for (int i2 = 0; i2 < items.Count; i2++)
                {
                    Console.Write(i2 + 1);
                    Console.Write(' ');
                    Console.WriteLine(items[i2]);
                }
                var answer = Console.ReadKey(false).KeyChar - '1';

                bool correctness = answer == items.IndexOf(question.answer);
                qs.Answer(correctness);
                Console.WriteLine(correctness);
                Console.ReadKey();

            }

            Console.ReadKey();
            foreach (var item in questions)
            {
                Console.WriteLine(item.answer);
                foreach (var item2 in item.dummies)
                {
                    Console.WriteLine(item2);
                }
                Console.WriteLine();
            }
            Console.ReadKey();


        }
    }
}
