﻿using ConsoleBuild;
using pidroh.conceptmap.model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClozeBase
{
    public class QuestionRunner
    {
        private ClozeQuestion[] questions;
        public QuestionScheduler qs;

        public QuestionRunner(string json)
        {
            Init(json);
        }

        public void Init(string json) {
            var map = ConceptMapDataUtils.LoadFromJson(json);
            questions = new ClozeGenerationMap().GetQuestions(map);
            qs = new QuestionScheduler();
            qs.Initialize(questions.Length);
        }

        internal ClozeQuestion GetQuestion(int qid)
        {
            return questions[qid];
        }
    }
}
