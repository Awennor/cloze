﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pidroh.conceptmap.model
{
    public static class ConceptMapDataUtils
    {
        //private static readonly fsSerializer _serializer = new fsSerializer();
        //private static fsData data;
        public static ConceptMapData LoadFromJson(string json) {

            //var dataOld = data;
            //fsJsonParser.Parse(json, out data);
            //Console.Write(dataOld == data);
            
            ConceptMapData mapData = null;
            mapData = JsonConvert.DeserializeObject<ConceptMapData>(json);
            //var bytes = MessagePackSerializer.FromJson(json);
            //mapData = MessagePackSerializer.Deserialize<ConceptMapData>(bytes);
            //_serializer.TryDeserialize<ConceptMapData>(data, ref mapData);


            return mapData;
        }

        public static string SaveToJson(ConceptMapData map)
        {

            //var dataOld = data;
            //_serializer.TrySerialize<ConceptMapData>(map, out data);
            var json = JsonConvert.SerializeObject(map);
            return json;

            //return fsJsonPrinter.PrettyJson(data);
        }
    }
}
