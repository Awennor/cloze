﻿using Bridge;
using Bridge.Html5;
using Bridge.jQuery2;
using ClozeBase;
using ConsoleBuild;
using Newtonsoft.Json;
using pidroh.conceptmap.model;
using Pidroh.BaseUtils;
using System;

namespace JSBuild
{
    public class App
    {

        public static void Main()
        {
            // Write a message to the Console
            //Console.WriteLine("Welcome to Bridge.NET");

            

            var div = new HTMLDivElement();
            var buttons = new HTMLButtonElement[5];
            HTMLParagraphElement prompt = new HTMLParagraphElement()
            {
                InnerHTML = "Prompt"
            };
            div.AppendChild(prompt);
            QuestionRunner qr = null;

            jQuery.Ajax(new AjaxOptions
            {
                MimeType = "mimeType: 'text/plain; charset=x-user-defined'",
                Url = "shared/komodo.txt",
                Success = delegate (object data, string str, jqXHR jqxhr) {
                    //Console.Write(data.ToString());
                    qr = new QuestionRunner(data.ToString());
                    
                    ClozeQuestion clozeQuestion = qr.GetQuestion(0);
                    prompt.InnerHTML = clozeQuestion.prompt;
                    var iutems = clozeQuestion.GetRandomizedItems();
                    for (int i = 0; i < buttons.Length; i++)
                    {
                        buttons[i].InnerHTML = iutems[i];
                    }
                    
                },
                Error = delegate (jqXHR d1, string d2, string d3) {
                    prompt.InnerHTML = "errorr";
                }
            });

            Random rnd = new Random();
            RandomSupplier.Generate = () =>
            {
                return (float)rnd.NextDouble();
            };

            
            
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i] = new HTMLButtonElement()
                {
                    Id = "b",
                    InnerHTML = "Button " + i
                    
                };
                var bu = buttons[i];
                int buttonId = i;
                bu.AddEventListener("click", () => {
                    qr.qs.Answer();
                });

                div.AppendChild(bu);
                div.AppendChild(new HTMLParagraphElement());
            }

            var buttonSave = new HTMLButtonElement()
            {
                Id = "b",
                InnerHTML = "Save"
            };

            var buttonRestore = new HTMLButtonElement()
            {
                Id = "r",
                InnerHTML = "Restore",
                Style =
        {
            Margin = "5px"
        }
            };
            buttonRestore.AddEventListener("click", ()=> {
                Console.Write("BLSA");
            });

            //div.AppendChild(input);
            div.AppendChild(buttonSave);
            div.AppendChild(buttonRestore);
            Document.Body.AppendChild(div);

            // After building (Ctrl + Shift + B) this project, 
            // browse to the /bin/Debug or /bin/Release folder.

            // A new bridge/ folder has been created and
            // contains your projects JavaScript files. 

            // Open the bridge/index.html file in a browser by
            // Right-Click > Open With..., then choose a
            // web browser from the list

            // This application will then run in the browser.
        }
    }
}